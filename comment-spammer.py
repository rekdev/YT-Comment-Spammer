#This script is licenced by GPL 3 LICENCE

#Import libraries
import os

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import googleapiclient.discovery 
import googleapiclient.errors

print("+++Spammeador de comentarios+++")
print("---------------")

#API access
flow = InstalledAppFlow.from_client_secrets_file(
    "client-secrets.json", scopes=["https://www.googleapis.com/auth/youtube.force-ssl"]
)

flow.run_local_server(port=8080, prompt="consent") 
credentials = flow.credentials

#Build YouTube service
youtube = googleapiclient.discovery.build("youtube", "v3", credentials=credentials)

print("---------------")

#Data required to post comments
cont = 0    

video_id = input("ID del video: ")
comment = input("Comentario: ")
comments_num = input("Numero de comentarios: ")

comments_num = float(comments_num)

print("-----------")
while(cont < comments_num):
    cont = cont + 1

    request = youtube.commentThreads().insert(
        part="snippet",
        body={
            "snippet": {
                "videoId": video_id,
                "topLevelComment": {
                    "snippet": {
                        "textOriginal": comment,
                    }
                }
            }
        }
    )

    response = request.execute()
    print(request)

print("-----------")

print("¡El proceso se completo con exito!")
